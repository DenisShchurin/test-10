package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shchurin.tm.api.*;
import ru.shchurin.tm.command.*;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.*;
import ru.shchurin.tm.service.*;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.HashUtil;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    private static final String GREETING = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskRepository);

    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private User currentUser;

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.shchurin.tm").getSubTypesOf(AbstractCommand.class);


    private void register(@Nullable final Class clazz) throws Exception {
        if (clazz == null) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();

        @Nullable final String cliCommand = command.getCommand();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void init() throws Exception {
        if (classes == null) return;
        for (@Nullable final Class clazz : classes)
            register(clazz);
    }

    public void initUsers() throws Exception {
        @NotNull final String adminHash = HashUtil.getHash("Admin");
        @NotNull final String userHash = HashUtil.getHash("User");

        @NotNull final User user = new User("User", userHash, Role.ROLE_USER);
        Project project = new Project("Project1", "project", new Date(), new Date(), user.getId());
        Task task = new Task("Task1", "description", project.getId(), new Date(), new Date(), user.getId());
        projectService.persist(project);
        taskService.persist(task);
        userService.persist(user);

        userService.persist(new User("Admin", adminHash, Role.ROLE_ADMIN));
//        userService.persist(new User("User", userHash, Role.ROLE_USER));
    }

    public void start() throws Exception {
        System.out.println(GREETING);
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        final boolean commandIsNotSafe = !abstractCommand.isSafe();
        if (commandIsNotSafe && currentUser == null)
            throw new UserNotAuthorized();

        final boolean userAuthorizedAndHasNotAccessRight = currentUser != null &&
                !abstractCommand.getRoles().contains(currentUser.getRole());
        if (commandIsNotSafe && userAuthorizedAndHasNotAccessRight) throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable User currentUser) {
        this.currentUser = currentUser;
    }
}
