package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ProjectService;
import ru.shchurin.tm.api.TaskRepository;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.api.ProjectRepository;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.List;

public final class ProjectServiceImpl implements ProjectService {
    @NotNull
    private final ProjectRepository projectRepository;

    @NotNull
    private final TaskRepository taskRepository;

    public ProjectServiceImpl(@NotNull ProjectRepository projectRepository, @NotNull TaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public List<Project> findAll(){
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        return projectRepository.findAll(userId);
    }

    @NotNull
    @Override
    public Project findOne(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            ProjectNotFoundException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (id == null || id.isEmpty())
            throw new ConsoleIdException();
        @Nullable final Project project = projectRepository.findOne(userId, id);
        if (project == null)
            throw  new ProjectNotFoundException();
        return project;
    }

    @Override
    public void persist(@Nullable final Project project) throws Exception {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getDescription() == null || project.getDescription().isEmpty())
            throw new ConsoleDescriptionException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.persist(project);
    }

    @Override
    public void merge(@Nullable final Project project) throws Exception {
        if (project == null)
            return;
        if (project.getName() == null || project.getName().isEmpty())
            throw new ConsoleNameException();
        if (project.getDescription() == null || project.getDescription().isEmpty())
            throw new ConsoleDescriptionException();
        if (project.getStartDate() == null)
            throw new ConsoleStartDateException();
        if (project.getEndDate() == null)
            throw new ConsoleEndDateException();
        projectRepository.merge(project);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String id) throws ConsoleIdException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty()) throw new UserNotAuthorized();
        if (id == null || id.isEmpty()) throw new ConsoleIdException();
        projectRepository.remove(userId, id);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        projectRepository.removeAll(userId);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) throws ConsoleNameException,
            UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public List<Task> getTasksOfProject(@Nullable final String userId, @Nullable final String name)
            throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Task> projectTasks = new ArrayList<>();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        @Nullable String projectId = null;
        for (@NotNull final Project project : projects) {
            if (project.getName().equals(name))
                projectId = project.getId();
        }

        @NotNull final List<Task> allTasks = taskRepository.findAll(userId);
        for (@NotNull final Task task : allTasks) {
            if (task.getProjectId().equals(projectId))
                projectTasks.add(task);
        }
        return projectTasks;
    }

    @Override
    public void removeProjectAndTasksByName(@Nullable final String userId, @Nullable final String name)
            throws ConsoleNameException, UserNotAuthorized {
        if (userId == null || userId.isEmpty())
            throw new UserNotAuthorized();
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        String projectId = null;
        for (@NotNull final Project project : projects) {
            if (project.getName().equals(name))
                projectId = project.getId();
        }
        if (projectId == null) return;

        @NotNull final List<Task> tasks = taskRepository.findAll(userId);
        for (@NotNull final Task task : tasks) {
            if (task.getProjectId().equals(projectId))
                taskRepository.remove(userId, task.getId());
        }
        projectRepository.removeByName(userId, name);
    }

    @NotNull
    @Override
    public List<Project> findByName(@Nullable final String name, @NotNull final String currentUserId)
            throws ConsoleNameException {
        if (name == null || name.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> allProjects = projectRepository.findAll(currentUserId);
        @NotNull final List<Project> projects = new ArrayList<>();
        for(@NotNull final Project project : allProjects) {
            if (project.getName().contains(name)) {
                projects.add(project);
            }
        }
        return projects;
    }

    @NotNull
    @Override
    public List<Project> findByDescription(@Nullable final String description, @NotNull final String currentUserId)
            throws ConsoleNameException {
        if (description == null || description.isEmpty())
            throw new ConsoleNameException();
        @NotNull final List<Project> allProjects = projectRepository.findAll(currentUserId);
        @NotNull final List<Project> projects = new ArrayList<>();
        for(@NotNull final Project project : allProjects) {
            if (project.getDescription().contains(description)) {
                projects.add(project);
            }
        }
        return projects;
    }
}
