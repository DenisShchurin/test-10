package ru.shchurin.tm.entity;

import org.jetbrains.annotations.NotNull;

public enum Status {
    STATUS_SCHEDULED,
    STATUS_IN_PROGRESS,
    STATUS_DONE;

    public void displayName(){
        for (@NotNull final Status status : Status.values()) {
            System.out.println(status);
        }
    }
}

