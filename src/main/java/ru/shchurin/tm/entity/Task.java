package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.util.DateUtil;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date startDate;

    @Nullable
    private Date endDate;

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    @Nullable
    private Date creationDate = new Date();

    public Task(@Nullable String id, @Nullable String name, @Nullable String description, @Nullable String projectId,
                @Nullable Date startDate, @Nullable Date endDate, @Nullable String userId, @Nullable Date creationDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectId = projectId;
        this.userId = userId;
        this.creationDate = creationDate;
    }

    public Task(@Nullable String name, @Nullable String description, @Nullable String projectId,
                @Nullable Date startDate, @Nullable Date endDate, @Nullable String userId) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Task{" +
                ", ID='" + id + '\'' +
                "task name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", projectId='" + projectId + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", creationDate=" + creationDate +
                '}';
    }
}
