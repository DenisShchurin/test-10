package ru.shchurin.tm.entity;

import org.jetbrains.annotations.NotNull;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN;

    @NotNull
    public String displayName() {
        return name();
    }
}
