package ru.shchurin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Project;

import java.util.Comparator;

public final class ProjectCreationComparator implements Comparator<Project> {
    @Override
    public int compare(@NotNull Project o1, @NotNull Project o2) {
        return o1.getCreationDate().compareTo(o2.getCreationDate());
    }
}

