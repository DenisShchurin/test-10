package ru.shchurin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Project;

import java.util.Comparator;

public final class ProjectStatusComparator implements Comparator<Project> {
    @Override
    public int compare(@NotNull Project o1, @NotNull Project o2) {
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
