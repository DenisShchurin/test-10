package ru.shchurin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class HashUtil {
    @NotNull
    public static String getHash(@NotNull final String password) throws NoSuchAlgorithmException {
        @NotNull final MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        @NotNull final byte[] digest = md.digest();
        return DatatypeConverter.printHexBinary(digest).toLowerCase();
    }
}
