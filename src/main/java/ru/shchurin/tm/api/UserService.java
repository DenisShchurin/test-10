package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;
import ru.shchurin.tm.exception.ConsoleHashPasswordException;
import ru.shchurin.tm.exception.ConsoleLoginException;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.List;

public interface UserService extends Service<User> {
    @NotNull
    List<User> findAll();

    @Nullable
    User findOne(@Nullable String id) throws Exception;

    void remove(@Nullable String id) throws Exception;

    void removeAll();

    void removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    String getHashOfPassword(@Nullable String password) throws Exception;

    @Nullable
    User authoriseUser(@Nullable String login, @Nullable String hashOfPassword) throws Exception;

    boolean updatePassword(@Nullable String login, @Nullable String hashPassword, @Nullable String newHashPassword)
            throws Exception;
}
