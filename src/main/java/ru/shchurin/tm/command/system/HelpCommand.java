package ru.shchurin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public final class HelpCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "help";

    @NotNull
    private static final String DESCRIPTION = "Show all commands.";

    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : ((Bootstrap)serviceLocator).getCommands()) {
            System.out.println(command.getCommand() + ": " + command.getDescription());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
