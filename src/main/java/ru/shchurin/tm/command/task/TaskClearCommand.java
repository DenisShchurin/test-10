package ru.shchurin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskClearCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "task-clear";

    @NotNull
    private static final String DESCRIPTION = "Remove all tasks.";

    @NotNull
    private static final String REMOVED = "[ALL TASK REMOVED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute () throws Exception {
        serviceLocator.getTaskService().removeAll(((Bootstrap)serviceLocator).getCurrentUser().getId());
        System.out.println(REMOVED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
