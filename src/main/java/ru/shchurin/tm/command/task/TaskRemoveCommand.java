package ru.shchurin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRemoveCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "task-remove";

    @NotNull
    private static final String DESCRIPTION = "Remove selected task.";

    @NotNull
    private static final String ENTER_TASK_NAME = "ENTER TASK NAME:";

    @NotNull
    private static final String REMOVED = "[TASKS REMOVED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(ENTER_TASK_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        try {
            serviceLocator.getTaskService().removeByName(((Bootstrap)serviceLocator).getCurrentUser().getId(), name);
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(REMOVED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
