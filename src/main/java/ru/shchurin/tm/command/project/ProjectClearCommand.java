package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectClearCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "project-clear";

    @NotNull
    private static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    private static final String ALL_PROJECTS_REMOVED = "[ALL PROJECTS REMOVED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        serviceLocator.getProjectService().removeAll(((Bootstrap)serviceLocator).getCurrentUser().getId());
        System.out.println(ALL_PROJECTS_REMOVED);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
