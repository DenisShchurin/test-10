package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectFindByDescriptionCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "project-find-by-description";

    @NotNull
    private static final String DESCRIPTION = "Find project by part of the description.";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(FIND_PROJECT);
        System.out.println(ENTER_DESCRIPTION);
        @Nullable final String description = ConsoleUtil.getStringFromConsole();
        @NotNull final String currentUserId = ((Bootstrap) serviceLocator).getCurrentUser().getId();

        @NotNull final List<Project> projects = serviceLocator.getProjectService().findByDescription(description, currentUserId);
        for (@NotNull final Project project : projects) {
            System.out.println(project);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
