package ru.shchurin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ProjectListCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "project-list";

    @NotNull
    private static final String DESCRIPTION = "Show all projects.";

    @NotNull
    private static final String PROJECT_LIST = "[PROJECT LIST]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println(PROJECT_LIST);
        int index = 1;
        for (@NotNull final Project project : serviceLocator.getProjectService().findAll(currentUser.getId())) {
            if (project.getUserId().equals(currentUser.getId())) {
                System.out.println(index++ + ". " + project);
            }
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
