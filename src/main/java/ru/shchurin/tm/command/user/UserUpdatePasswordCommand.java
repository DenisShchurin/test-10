package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.List;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "user-show-profile";

    @NotNull
    private static final String DESCRIPTION = "Show user profile.";

    @NotNull
    private static final String PASSWORD_UPDATE = "[USER PASSWORD UPDATE]";

    @NotNull
    private static final String PASSWORD_UPDATED = "[USER PASSWORD UPDATED]";

    @NotNull
    private static final String PASSWORD_NOT_UPDATED = "[USER PASSWORD IS NOT UPDATED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println(PASSWORD_UPDATE);
        System.out.println(ENTER_PASSWORD);
        @Nullable final String newPassword = ConsoleUtil.getStringFromConsole();
        @NotNull final String newHashPassword = serviceLocator.getUserService().getHashOfPassword(newPassword);
        final boolean update = serviceLocator.getUserService().updatePassword(currentUser.getLogin(),
                currentUser.getHashPassword(), newHashPassword);
        if (update) {
            System.out.println(PASSWORD_UPDATED);
        } else {
            System.out.println(PASSWORD_NOT_UPDATED);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
