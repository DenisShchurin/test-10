package ru.shchurin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.Role;

import java.util.ArrayList;
import java.util.List;

public final class UserEndSessionCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "end-session";

    @NotNull
    private static final String DESCRIPTION = "End user session";

    @NotNull
    private static final String END_SESSION = "[USER END SESSION]";

    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println(END_SESSION);
        ((Bootstrap)serviceLocator).setCurrentUser(null);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
