package ru.shchurin.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class FasterXmlSaveXmlCommand extends AbstractCommand {
    private static final String COMMAND = "f-s-x";
    private static final String DESCRIPTION = "Save application data.";
    private static final String SAVING = "[SAVE APPLICATION DATA]";

    @NotNull
    private static final String FILE = "C:\\Users\\user\\IdeaProjects\\Data\\fasterxml.xml";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(SAVING);
        @NotNull final ObjectMapper mapper = new XmlMapper();
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        @NotNull final SavedApplication savedApplication = new SavedApplication(projects, tasks, users);
        @NotNull final File file = new File(FILE);
        mapper.writerWithDefaultPrettyPrinter().writeValue(file, savedApplication);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
