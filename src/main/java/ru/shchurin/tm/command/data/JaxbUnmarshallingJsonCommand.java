package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.*;

public final class JaxbUnmarshallingJsonCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "j-u-j";
    private static final String DESCRIPTION = "Unmarshalling application data.";
    private static final String UNMARSHALLING = "[UNMARSHALLING APPLICATION DATA ]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(UNMARSHALLING);
        System.setProperty("javax.xml.bind.context.factory","org.eclipse.persistence.jaxb.JAXBContextFactory");
        @NotNull final Map<String, Object> properties = new HashMap<>();
        properties.put("eclipselink.media-type", "application/json");
        @NotNull final Class[] classes = new Class[] {SavedApplication.class};
        @NotNull final File file = new File("C:\\Users\\user\\IdeaProjects\\Data\\jaxb.json");
        @NotNull final JAXBContext context = JAXBContext.newInstance(classes, properties);
        @NotNull final Unmarshaller unmarshaller = context.createUnmarshaller();
        @NotNull final SavedApplication savedApplication = (SavedApplication) unmarshaller.unmarshal(file);

        if (savedApplication.getProjects() != null)
        for (@NotNull final Project project : savedApplication.getProjects()) {
            serviceLocator.getProjectService().persist(project);
        }

        if (savedApplication.getTasks() != null)
        for (@NotNull final Task task : savedApplication.getTasks()) {
            serviceLocator.getTaskService().persist(task);
        }

        if (savedApplication.getUsers() != null)
        for (@NotNull final User user : savedApplication.getUsers()) {
            serviceLocator.getUserService().persist(user);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
