package ru.shchurin.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.*;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class DataSerializationCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "d-s";

    @NotNull
    private static final String DESCRIPTION = "Serialize application data.";

    @NotNull
    private static final String SERIALIZE = "[SERIALIZE APPLICATION DATA ]";

    @NotNull
    private static final String FILE = "C:\\Users\\user\\IdeaProjects\\Data\\save.txt";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(SERIALIZE);
        @NotNull final List<Project> projects = serviceLocator.getProjectService().findAll();
        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findAll();
        @NotNull final List<User> users = serviceLocator.getUserService().findAll();
        @NotNull final SavedApplication savedApplication = new SavedApplication(projects, tasks, users);

        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(savedApplication);
        objectOutputStream.close();
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
