package ru.shchurin.tm.exception;

public final class UserNotAuthorized extends Exception {
    public UserNotAuthorized() {
        super("You have not authorized");
    }
}
