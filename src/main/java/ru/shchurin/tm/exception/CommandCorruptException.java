package ru.shchurin.tm.exception;

public final class CommandCorruptException extends Exception {
    public CommandCorruptException() {
        super("DESCRIPTION MUST NOT BE NULL OR EMPTY");
    }
}
