package ru.shchurin.tm.exception;

public final class AlreadyExistsException extends Exception{
    public AlreadyExistsException() {
        super("THIS ENTITY ALREADY EXISTS");
    }
}
