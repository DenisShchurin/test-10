package ru.shchurin.tm.exception;

public final class ConsoleHashPasswordException extends Exception {
    public ConsoleHashPasswordException() {
        super("YOU ENTERED INCORRECT PASSWORD");
    }
}
